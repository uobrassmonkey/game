using System;
using System.Collections.Generic;

namespace WatsonAI
{
  /// <summary>
  /// Creates a Knowledge object and its Associations based on hard-coded story
  /// </summary>
  public static class Story
  {
    public enum Names : int { ACTRESS, BUTLER, COLONEL, COUNTESS, EARL, GANGSTER, POLICE };
    public static Dictionary<Names, Character> Characters { get; }
    public static Knowledge Knowledge;
    public static Associations Associations;
    public static EntityBuilder entities;
    public static List<string> whoEntities;
    public static List<string> whatEntities;
    public static VerbBuilder verbs;

    // HS: Single line comments are entities/verbs/relations that I think
    //     might need hiding/omitting from the graphs.
    //     Multi line comments are entities/verbs/relations that I am unsure
    //     how to represent.
    static Story()
    {
      entities = new EntityBuilder {
        "actress", "butler", "countess", "earl",
        "gangster", "colonel", "scrap", "nightshade", "belongings", "fast-acting",
        "blackcurrants","blueberries", "dining_room", "letter", "master_bedroom",
        "arsenic", "rat_poison", "kitchen", "plants", "nervous", "barbital_tolerance",
        "barbital", "sleeping_pills", "bathroom", "book", "estate", "promotion",
        "war", "note", "contents", "will", "desk", "study", "slow-acting","herbology", 
        "daughter", "money", "allergy", "tolerance","murderer"
      };

      whoEntities = new List<string> {
        "actress", "butler" , "countess", "earl", "gangster", "colonel", "earl", "murderer", "daughter"
      };
      whatEntities = new List<string>
      {
        "scrap", "nightshade", "belongings", "fast-acting",
        "blackcurrants","blueberries", "dining_room", "letter", "master_bedroom",
        "arsenic", "rat_poison", "kitchen", "plants", "nervous", "barbital_tolerance",
        "barbital", "sleeping_pills", "bathroom", "book", "estate", "promotion",
        "war", "note", "contents", "will", "desk", "study", "slow-acting","herbology",
        "money", "allergy", "tolerance",
      };
      var verbs = new VerbBuilder {
        "study", "have", "about", "contain", "own", "poison", "on", "fight",
        "resent", "want", "prevent", "marry", "use", "employ", "owe", "meet",
        "send", "receive", "be", "steal", "get", "kill", "murder", "inherit"
      };
      var universeKnowledgeBuilder = new KnowledgeBuilder(entities, verbs)
      {
        {"actress", "kill","earl"},
        {"actress", "be", "murderer"},
        {"actress", "be", Object.Direct("daughter"), Object.Indirect("of", "earl")},
        {"actress", "study", "herbology"},
        {"actress", "have", "scrap"},
        {"scrap", "about", "nightshade"},
        {"nightshade", "kill", "earl"},
        //{"belongings", "contain", "nightshade"},
        {"colonel", "own", "belongings"},
        {"nightshade", "be", "fast-acting"},
        {"arsenic", "be", "slow-acting"},
        /*{"nightshade", "look like", "blackcurrants"},*/
        {"dining_room", "contain", "blackcurrants"},
        {"dining_room", "contain", "blueberries"},
        {"nightshade", "poison", "earl"},
        {"actress", "inherit", "money"},
        {"actress", "get", Object.Direct("contents"), Object.Indirect("of", "will")},
        {"will", "on", "desk"},
        {"study", "contain", "desk"},
        {"study", "contain", "will"},
        {"earl", "fight", "war"},
        /*{"earl", "friends with", "colonel"},*/ //Probably just a like relation
        {"colonel", "fight", "war"},
        {"colonel", "resent", "earl"},
        {"colonel", "want", "promotion"},
        {"earl", "prevent", "promotion"},
        {"note", "about", "promotion"},
        {"earl", "marry", "countess"},
        {"countess", "marry", "earl"},
        /*{"countess", "wants to sell", "estate"},*/
        {"earl", "use", "sleeping_pills"},
        {"book", "about", "barbital"},
        {"study", "contain", "book"},
        {"barbital", "be", "sleeping_pills"},
        {"bathroom", "contain", "barbital"},
        {"earl", "have", Object.Direct("tolerance"), Object.Indirect("to", "barbital")},
        {"earl", "employ", "butler"},
        {"butler", "steal", Object.Direct("money"), Object.Indirect("from", "earl")},
        {"butler", "be", "nervous"},
        {"butler", "have", Object.Direct("allergy"), Object.Indirect("to", "plants")},
        {"butler", "have", "rat_poison"},
        {"kitchen", "contain", "rat_poison"},
        {"earl", "owe", "gangster"},
        {"earl", "meet", "gangster"},
        {"gangster", "meet", "earl"},
        /*{"gangster", "kill with", "arsenic"},*/
        /*{"arsenic, "used as", "rat poison"},*/
        {"master_bedroom", "contain", "letter"},
        {"dining_room", "contain", "butler"},
        {"gangster", "send", "letter"},
        {"earl", "receive", "letter"},


      };
      Associations = universeKnowledgeBuilder.Associations;
      Knowledge = universeKnowledgeBuilder.Knowledge;

      Characters = new Dictionary<Names, Character>
      {
        {Names.ACTRESS, new Character("actress", true, Gender.Female, "drawing room")},
        {Names.COUNTESS, new Character("countess", false, Gender.Female, "master bedroom")},
        {Names.COLONEL, new Character("colonel", false, Gender.Male, "study")},
        {Names.GANGSTER, new Character("gangster", false, Gender.Male, "billiard's room")},
        {Names.POLICE, new Character("policeman", false, Gender.Male, "entrance-way")},
        {Names.BUTLER, new Character("butler", false, Gender.Male, "dining room")},
      };

      Characters[Names.ACTRESS].AddMood("I'm an actress");
      Characters[Names.ACTRESS].AddSeen("I saw the earl collapse, not much else to say");
      Characters[Names.ACTRESS].AddGreeting("Good evening Detective");

      Characters[Names.COUNTESS].AddMood("I'm devastated");
      Characters[Names.COUNTESS].AddSeen("I saw the earl die so quickly, it was horrible!");
      Characters[Names.COUNTESS].AddGreeting("How can I help?");

      Characters[Names.COLONEL].AddMood("I'm pulling through");
      Characters[Names.COLONEL].AddSeen("Why the man just collapsed, never seen anything like it");
      Characters[Names.COLONEL].AddGreeting("All right, old chap?");

      Characters[Names.GANGSTER].AddMood("I'm fine. Nothing I haven't seen before");
      Characters[Names.GANGSTER].AddSeen("The old geezer choked and fell down");
      Characters[Names.GANGSTER].AddGreeting("What do you need me for?");

      Characters[Names.POLICE].AddMood("I'm a policeman");
      Characters[Names.POLICE].AddSeen("I'm a policeman");

      Characters[Names.BUTLER].AddMood("I'm a tad shaken up");
      Characters[Names.BUTLER].AddSeen("The poor master collapsed suddenly, there was nothing I could do");
      Characters[Names.BUTLER].AddGreeting("Good day sir");

      //var characterKnowledgeBuilders = new List<KnowledgeBuilder>
      var characterKnowledgeBuilders = new Dictionary<Names, KnowledgeBuilder>
      {
        {
          Names.ACTRESS,
          new KnowledgeBuilder(entities, verbs)
          {

            {"actress", "be", Object.Direct("daughter"), Object.Indirect("of", "earl")},
            {"actress", "study", "herbology"},
            {"actress", "have", "scrap"},
            {"scrap", "about", "nightshade"},
            //{"belongings", "contain", "nightshade"},
            //{"colonel", "own", "belongings"},
            {"nightshade", "be", "fast-acting"},
            /*{"nightshade", "look like", "blackcurrants"},*/
            {"dining_room", "contain", "blackcurrants"},
            {"dining_room", "contain", "blueberries"},
            {"nightshade", "poison", "earl"},
            {"actress", "inherit", "money"},
            {"actress", "get", Object.Direct("contents"), Object.Indirect("of", "will")},
            {"earl", "fight", "war"},
            /*{"earl", "friends with", "colonel"},*/ //Probably just a like relation
            {"colonel", "fight", "war"},
            {"earl", "marry", "countess"},
            {"countess", "marry", "earl"},
            {"barbital", "be", "sleeping_pills"},
            //{"arsenic, "used as", "rat poison"},*/
            {"earl", "employ", "butler"}
          }
        },
        {
          Names.BUTLER,
          new KnowledgeBuilder(entities, verbs)
          {
            /*{"nightshade", "look like", "blackcurrants"},*/
            {"dining_room", "contain", "blackcurrants"},
            {"dining_room", "contain", "blueberries"},
            {"nightshade", "be", "fast-acting"},
            {"arsenic", "be", "slow-acting"},
            {"dining_room", "contain", "butler"},
            {"will", "on", "desk"},
            {"study", "contain", "desk"},
            {"study", "contain", "will"},
            {"earl", "fight", "war"},
            /*{"earl", "friends with", "colonel"},*/ //Probably just a like relation
            {"colonel", "fight", "war"},
            {"countess", "inherit", "money"},
            {"countess", "get", Object.Direct("contents"), Object.Indirect("of", "will")},
            {"colonel", "want", "promotion"},
            {"earl", "marry", "countess"},
            {"countess", "marry", "earl"},
            /*{"countess", "wants to sell", "estate"},*/
            {"book", "about", "barbital"},
            {"study", "contain", "book"},
            {"earl", "employ", "butler"},
            // {"butler", "steal", Object.Direct("money"), Object.Indirect("from", "earl")}, // needs hiding/omitting in this graph
            {"butler", "be", "nervous"},
            {"butler", "steal", Object.Direct("money"), Object.Indirect("from", "earl")},
            {"butler", "have", "rat_poison"},
            {"kitchen", "contain", "rat_poison"},
            {"earl", "owe", "gangster"},
            {"earl", "meet", "gangster"},
            {"gangster", "meet", "earl"},
            /*{"gangster", "kill with", "arsenic"},*/
            /*{"arsenic, "used as", "rat poison"},*/
            {"master_bedroom", "contain", "letter"},
            {"gangster", "send", "letter"},
            {"earl", "receive", "letter"},

          }
        },
        {
          Names.COLONEL,
          new KnowledgeBuilder(entities, verbs)
          {
            {"actress", "be", Object.Direct("daughter"), Object.Indirect("of", "earl")},
            //{"colonel", "own", "belongings"},
            {"nightshade", "be", "fast-acting"},
            /*{"dining room", "contain", "blackcurrants"},*/
            {"countess", "inherit", "money"},
            {"countess", "get", Object.Direct("contents"), Object.Indirect("of", "will")},
            {"will", "on", "desk"},
            {"study", "contain", "desk"},
            {"study", "contain", "will"},
            {"earl", "fight", "war"},
            /*{"earl", "friends with", "colonel"},*/ //Probably just a like relation
            {"colonel", "fight", "war"},
            {"colonel", "resent", "earl"},
            {"colonel", "want", "promotion"},
            {"earl", "prevent", "promotion"},
            {"earl", "marry", "countess"},
            {"countess", "marry", "earl"},
            {"earl", "employ", "butler"},
            {"butler", "have", "rat_poison"},
             /*{"arsenic, "used as", "rat poison"},*/
            {"earl", "owe", "gangster"}
          }
        },
        {
          Names.COUNTESS,
          new KnowledgeBuilder(entities, verbs)
          {
            {"actress", "study", "herbology"},
            {"dining_room", "contain", "blackcurrants"},
            {"dining_room", "contain", "blueberries"},
            {"countess", "inherit", "money"},
            {"countess", "get", Object.Direct("contents"), Object.Indirect("of", "will")},
            {"will", "on", "desk"},
            {"study", "contain", "desk"},
            {"study", "contain", "will"},
            {"earl", "fight", "war"},
            /*{"earl", "friends with", "colonel"},*/ //Probably just a like relation
            {"colonel", "fight", "war"},
            {"colonel", "resent", "earl"},
            {"colonel", "want", "promotion"},
            {"earl", "prevent", "promotion"},
            {"note", "about", "promotion"},
            {"earl", "marry", "countess"},
            {"countess", "marry", "earl"},
            /*{"countess", "wants to sell", "estate"},*/
            {"earl", "use", "sleeping_pills"},
            {"barbital", "be", "sleeping_pills"},
            {"earl", "have", Object.Direct("tolerance"), Object.Indirect("to", "barbital")},
            {"earl", "employ", "butler"},
            {"butler", "be", "nervous"},
            /*{"butler", "have", "rat poison"},*/
            /*{"kitchen", "contain", "rat poison"},*/
            {"earl", "meet", "gangster"},
            {"gangster", "meet", "earl"},
            /*{"arsenic, "used as", "rat poison"},*/
            {"master_bedroom", "contain", "letter"},
            {"earl", "receive", "letter"}
          }
        },
        {
          Names.EARL,
          new KnowledgeBuilder(entities, verbs)
        },
        {
          Names.GANGSTER,
          new KnowledgeBuilder(entities, verbs)
          {
            {"nightshade", "be", "fast-acting"},
            {"dining_room", "contain", "blackcurrants"},
            {"dining_room", "contain", "blueberries"},
            /*{"earl", "friends with", "colonel"},*/ //Probably just a like relation
            {"colonel", "fight", "war"},
            {"earl", "marry", "countess"},
            {"countess", "marry", "earl"},
            {"countess", "inherit", "money"},
            {"butler", "steal", Object.Direct("money"), Object.Indirect("from", "earl")},
            {"butler", "be", "nervous"},
            {"earl", "owe", "gangster"},
            {"earl", "meet", "gangster"},
            {"gangster", "meet", "earl"},
            /*{"gangster", "kill with", "arsenic"},*/
            {"arsenic", "be", "slow-acting"},
            /*{"arsenic, "used as", "rat poison"},*/
            {"master_bedroom", "contain", "letter"},
            {"gangster", "send", "letter"},
            {"earl", "receive", "letter"}
          }
        },
        {
          Names.POLICE,
          new KnowledgeBuilder(entities, verbs)
        }
      };

      foreach (var c in Characters)
      {
        c.Value.Knowledge = characterKnowledgeBuilders[c.Key].Knowledge;
      }
    }

    public static List<Entity> WhoEntityFilter(IEnumerable<Entity> entities) 
    {
      List<Entity> whoFilteredEntities = new List<Entity>();
      foreach (var entity in entities) 
      {
        foreach(var name in whoEntities)
        {
          string entityName;
          Associations.TryNameEntity(entity,out entityName);
          if (entityName == name) whoFilteredEntities.Add(entity); 
        }
      }
      return whoFilteredEntities;
    }

    public static List<Entity> WhatEntityFilter(IEnumerable<Entity> entities)
    {
      List<Entity> whatFilteredEntities = new List<Entity>();
      foreach (var entity in entities)
      {
        foreach (var name in whatEntities)
        {
          string entityName;
          Associations.TryNameEntity(entity, out entityName);

          if (entityName == name) whatFilteredEntities.Add(entity); 
        }
      }
      return whatFilteredEntities;
    }
  }
}