﻿using OpenNLP.Tools.Parser;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WatsonAI
{
  public class InputSimplificationProcess : IProcess
  {
    public Stream Process(Stream stream)
    {
      var replacing = new List<Tuple<List<string>, List<string>>>();

      replacing.AddRange(Replacements());

      ReplaceWords(replacing, stream.Input);

      return stream;
    }

    public List<Tuple<List<string>, List<string>>> Replacements()
    {
      var replacing = new List<Tuple<List<string>, List<string>>>
      {
        Tuple.Create(new List<string> { "is", "in" }, new List<string> { "is", "contained", "in" }),
        Tuple.Create(new List<string> { "studies" }, new List<string> { "study" })
      };
      return replacing;
    }

    private void ReplaceWords(List<Tuple<List<string>, List<string>>> replacements, List<string> tokens)
    {
      for (int i = 0; i < tokens.Count; i++)
      {
        foreach (var replacement in replacements)
        {
          var originalPhrase = replacement.Item1;
          var replacementPhrase = replacement.Item2;
          if (i + originalPhrase.Count < tokens.Count)
          {
            var tokenSection = tokens.GetRange(i, originalPhrase.Count);
            if (AllWordsEqual(originalPhrase, tokenSection))
            {
              tokens.RemoveRange(i, originalPhrase.Count);
              tokens.InsertRange(i, replacementPhrase);
            }
          }
        }
      }
    }

    private bool AllWordsEqual(List<string> firstTokens, List<string> secondTokens)
     => firstTokens.Zip(secondTokens, (x, y) => x.Equals(y, StringComparison.OrdinalIgnoreCase)).All(x => x);
  }
}
