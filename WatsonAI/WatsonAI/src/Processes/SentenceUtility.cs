﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WatsonAI
{
  public static class SentenceUtility
  {
    /// <summary>
    /// Converts a list of nouns into multiple noun sentence form: eg. [eggs, bacon, ham] => "eggs, bacon and ham".
    /// </summary>
    /// <returns></returns>
    public static string MultiNounSentence(List<string> tokens)
    {
      var sentence = new List<string>(tokens);

      for (int i = 0; i < tokens.Count - 2; i++)
      {
        sentence[i] += ",";
      }

      if (tokens.Count > 1)
      {
        sentence.Insert(sentence.Count - 1, "and");
      }
      return string.Join(" ", sentence);
    }
  }
}
