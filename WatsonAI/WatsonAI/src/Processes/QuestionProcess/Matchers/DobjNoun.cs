﻿using OpenNLP.Tools.Parser;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using static WatsonAI.Patterns;

namespace WatsonAI
{
  public class DobjNoun : IEntityMatcher
  {
    private readonly CommonPatterns cp;
    private readonly KnowledgeQuery query;
    private readonly Associations associations;
    private readonly Thesaurus thesaurus;

    private IEnumerable<Entity> answers = null;
    private string response = null;

    public DobjNoun(CommonPatterns cp, KnowledgeQuery query, Associations associations, Thesaurus thesaurus)
    {
      this.cp = cp;
      this.query = query;
      this.associations = associations;
      this.thesaurus = thesaurus;
    }

    public bool MatchOn(Parse tree)
    {
      var whoQuestion = (cp.Top >= (Branch("SBARQ") > Branch("WHNP"))).Flatten();
      //Debug.WriteLineIf(whoQuestion.Match(tree).HasValue, "Who Question");
      var passiveDobjQuestion = cp.Top >= (And(Branch("SQ") > cp.SimpleVerb, Branch("SQ") > Branch("NP")));
      //Debug.WriteLineIf(passiveSubjQuestion.Match(tree).HasValue, "Active Subj Question");
      var passiveDobjWho = And(whoQuestion, passiveDobjQuestion);
      var notVerb = cp.Top >= (Branch("SQ") > Branch("VP"));


      var isPassiveDobjWho = passiveDobjWho.Match(tree).HasValue && !notVerb.Match(tree).HasValue;
      Debug.WriteLineIf(isPassiveDobjWho, "Dobj Noun");

      var containsWho = cp.Top >= Word(thesaurus, "who");
      var containsWhat = cp.Top >= Word(thesaurus, "what");

      var patternWhoQuestion = And(containsWho, whoQuestion);
      var patternWhatQuestion = And(containsWhat, whoQuestion);

      var isWhoQuestion = patternWhoQuestion.Match(tree).HasValue;
      var isWhatQuestion = patternWhatQuestion.Match(tree).HasValue;

      if (isPassiveDobjWho)
      {
        var entityPattern = (cp.Top >= (Branch("SQ") > cp.NounPhrase)).Flatten().Flatten();
        var entities = entityPattern.Match(tree);

        var verbs = new List<Verb>();
        foreach (var e in entities.Value)
        {
          var temp = "";
          var tempVerb = new Verb();
          associations.TryNameEntity(e, out temp);
          if (associations.TryGetVerb(temp, out tempVerb))
          {
            verbs.Add(tempVerb);
          }
        }
        if (entities.HasValue && verbs.Count!= 0)
        {

          answers = GenerateAnswers(entities.Value.Distinct(), verbs.Distinct());
          if (isWhoQuestion) { answers = Story.WhoEntityFilter(answers); }
          if (isWhatQuestion) { answers = Story.WhatEntityFilter(answers); }
          if (answers.Any())
          {
            var entity = "";
            var verb = "";
            associations.TryNameEntity(entities.Value.First(), out entity);
            associations.TryNameVerb(verbs.First(), out verb);
           
            var answerTokens = answers.Select(a => associations.UncheckedNameEntity(a)).Distinct();
            var answerString = SentenceUtility.MultiNounSentence(answerTokens.ToList());
            var responseParts = new string[] { "The", entity, verb, answerString };
        
            response = string.Join(" ", responseParts);
            Debug.WriteLine("Response: " + response);
            return true;

          }
        }
      }

      return false;
    }

    public string GenerateResponse()
    {
      return response;
    }

    public IEnumerable<Entity> GetAnswers()
    {
      return answers;
    }

    private List<Entity> GenerateAnswers(IEnumerable<Entity> entities, IEnumerable<Verb> verbs)
    {
      var pairs = from e in entities
                  from v in verbs
                  select Tuple.Create(e, v);

      var answers = new List<Entity>();
      foreach (var p in pairs.Distinct())
      {
        var e = p.Item1;
        var v = p.Item2;
        answers.AddRange(query.GetDobjAnswers(v, e));
      }
      return answers;
    }
  }
}
