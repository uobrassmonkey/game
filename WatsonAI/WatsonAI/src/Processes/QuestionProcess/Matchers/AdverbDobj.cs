﻿using OpenNLP.Tools.Parser;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using static WatsonAI.Patterns;

namespace WatsonAI
{
  public class AdverbDobj : IEntityMatcher
  {
    private readonly CommonPatterns cp;
    private readonly KnowledgeQuery query;
    private readonly Associations associations;
    private readonly Thesaurus thesaurus;

    private IEnumerable<Entity> answers = null;
    private string response = null;

    public AdverbDobj(CommonPatterns cp, KnowledgeQuery query, Associations associations, Thesaurus thesaurus)
    {
      this.cp = cp;
      this.query = query;
      this.associations = associations;
      this.thesaurus = thesaurus;
    }

    public bool MatchOn(Parse tree)
    {
      var whoQuestion = (cp.Top >= (Branch("SBARQ") > Branch("WHNP"))).Flatten();
      //Debug.WriteLineIf(whoQuestion.Match(tree).HasValue, "Who Question");
      var activeDobjQuestion = cp.Top >= (And(Branch("SQ") > (Branch("VP") > Branch("NP")), And(Branch("SQ") > (Branch("VP") > cp.SimpleVerb), Branch("SQ") > (Branch("VP") > Branch("ADVP")))));
      //Debug.WriteLineIf(activeSubjQuestion.Match(tree).HasValue, "Active Subj Question");
      var activeDobjWho = And(whoQuestion, activeDobjQuestion);
      var containsWho = cp.Top >= Word(thesaurus, "who");
      var containsWhat = cp.Top >= Word(thesaurus, "what");

      var patternWhoQuestion = And(containsWho, whoQuestion);
      var patternWhatQuestion = And(containsWhat, whoQuestion);


      var isWhoQuestion = patternWhoQuestion.Match(tree).HasValue;
      var isWhatQuestion = patternWhatQuestion.Match(tree).HasValue;

      var isActiveDobjWho = activeDobjWho.Match(tree).HasValue;
      Debug.WriteLineIf(isActiveDobjWho, "Adverb Dobj");

      if (isActiveDobjWho)
      {
        var entityPattern = (cp.Top >= (Branch("SQ") > (Branch("VP") > cp.NounPhrase))).Flatten().Flatten().Flatten();
        var entities = entityPattern.Match(tree);

        var verbPattern = (cp.Top >= (Branch("SQ") > (Branch("VP") > cp.AdvPhrase))).Flatten().Flatten().Flatten();

        var verbs = verbPattern.Match(tree);
        if (entities.HasValue && verbs.HasValue)
        {
          answers = GenerateAnswers(entities.Value.Distinct(), verbs.Value.Distinct());
          if (isWhoQuestion) { answers = Story.WhoEntityFilter(answers); }
          if (isWhatQuestion) { answers = Story.WhatEntityFilter(answers); }
          if (answers.Any())
          {

            var answerTokens = answers.Select(a => associations.UncheckedNameEntity(a)).Distinct();
            var answerString = SentenceUtility.MultiNounSentence(answerTokens.ToList());

            var responseParts = new string[] { "The", answerString };

            response = string.Join(" ", responseParts);
            Debug.WriteLine("Response: " + response);
            return true;
          }
        }
      }

      return false;
    }

    public string GenerateResponse()
    {
      return response;
    }

    public IEnumerable<Entity> GetAnswers()
    {
      return answers;
    }

    private List<Entity> GenerateAnswers(IEnumerable<Entity> entities, IEnumerable<Verb> verbs)
    {
      var pairs = from e in entities
                  from v in verbs
                  select Tuple.Create(e, v);

      var answers = new List<Entity>();
      foreach (var p in pairs.Distinct())
      {
        var e = p.Item1;
        var v = p.Item2;
        answers.AddRange(query.GetDobjAnswers(v, e));
      }
      return answers;
    }
  }
}
