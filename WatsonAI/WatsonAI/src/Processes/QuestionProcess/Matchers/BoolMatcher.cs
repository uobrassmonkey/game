﻿namespace WatsonAI
{
  interface IBoolMatcher : IMatcher
  {
    bool GetAnswer();
  }
}
