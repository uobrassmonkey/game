﻿public static class Stats
{
    private static int score;
    private static float time;
    private static bool menu = false;
    private static string rank;

    public static int Score
    {
        get
        {
            return score;
        }
        set
        {
            score = value;
        }
    }

    public static string Rank
    {
        get
        {
            return rank;
        }
        set
        {
            rank = value;
        }
    }

    public static float Time
    {
        get
        {
            return time;
        }
        set
        {
            time = value;
        }
    }

    public static bool Menu
    {
        get
        {
            return menu;
        }
        set
        {
            menu = value;
        }
    }
}