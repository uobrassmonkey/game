﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndController : MonoBehaviour {

    public Text score;
    public Text time;

	// Use this for initialization
	void Start () {


        score.text = Stats.Rank;

        int minutes = (int)(Stats.Time / 60);
        int seconds = (int)Stats.Time%60;
        string secondsString = seconds.ToString();
        if (seconds < 10)
        {
            secondsString = "0" + secondsString;
        }
        time.text = minutes.ToString() + ":" + secondsString;
        
	}
	
	// Update is called once per frame
	void Update () {
        
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("Main_Menu", LoadSceneMode.Single);
    }
}
