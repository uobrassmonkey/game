﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;
using Notebook;
using NPC;

public class MasterControl : MonoBehaviour {

    public GameObject masterCanvas;
    public NotebookController notebook;
    public RigidbodyFirstPersonController fpc;
    public GameState state;
    public InputField notesInput;
    public bool inspect = false;
    public GameObject inspectClose;
    public DialogueScreen dialogue;

    // State variables
    public bool paused = false;
    private float launch;
    private float end;
    public bool finalForm = false;

    // Use this for initialization
    void Start () {
        launch = Time.realtimeSinceStartup;
        Debug.Log("Launch: " + launch.ToString());
        notebook.Activate(false);
    }

    void OpenNotebook(bool status)
    {
        notebook.Activate(status);
        Pause(status);
        notebook.showing = status;
        if (status)
        {
            state.OpenNotebook();
            Cursor.visible = status;
        }
    }

    public void CloseInspect()
    {
        if (!notebook.showing)
        {
            TogglePaused();
        }
        Debug.Log(notebook.showing);
        inspectClose.SetActive(false);
        notebook.currentInspect.GetComponent<Interactable>().HideInspect();
        notebook.inspect = false;
        inspect = false;
    }

	// Update is called once per frame
	void Update () {
        // TODO replace with switch
        if (!state.finalForm && !notebook.inspect && !inspect && state.pickup && !dialogue.isShowing())
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (!paused)
                {
                    notebook.ChangePage((int)Page.MENU);
                }
                OpenNotebook(!paused);
            }
            if (Input.GetKeyDown(KeyCode.C) && !notesInput.isFocused)
            {
                OpenNotebook(true);
                notebook.ChangePage((int)Page.CHARACTER);
            }
            if (Input.GetKeyDown(KeyCode.I) && !notesInput.isFocused)
            {
                OpenNotebook(true);
                notebook.ChangePage((int)Page.INVENTORY);
            }
            if (Input.GetKeyDown(KeyCode.N) && !notesInput.isFocused)
            {
                OpenNotebook(true);
                notebook.ChangePage((int)Page.NOTES);
            }
            if (Input.GetKeyDown(KeyCode.M) && !notesInput.isFocused)
            {
                OpenNotebook(true);
                notebook.ChangePage((int)Page.MAP);
            }
            if (Input.GetKeyDown(KeyCode.Tab) && !notesInput.isFocused)
            {
                OpenNotebook(!paused);
                notebook.ChangePage((int)notebook.currentPageEnum);
            }
        }
    }


    public void Pause(bool pause)
    {
        switch (pause)
        {
            case true:
                paused = true;
                masterCanvas.SetActive(false);
                Time.timeScale = 0;
                //fpc.mouseLook.SetCursorLock(false);
                //fpc.mouseLook.lockCursor = false;
                fpc.mouseLook.SetCursorLock(false);
                Cursor.visible = true;
                break;
            default:
                StartCoroutine(WaitThenUnpause());
                masterCanvas.SetActive(true);
                Time.timeScale = 1;
                //fpc.mouseLook.SetCursorLock(true);
                //fpc.mouseLook.lockCursor = true;
                fpc.mouseLook.SetCursorLock(true);
                Cursor.visible = false;
                break;
        }
    }

    public IEnumerator WaitThenUnpause()
    {
        yield return new WaitForEndOfFrame();
        paused = false;
    }

    public void EndGame(int score)
    {
        end = Time.realtimeSinceStartup;
        Debug.Log("End: " + end.ToString());
        Stats.Time = end - launch;
        Stats.Score = score;
        Stats.Rank = state.GetRank(score);
        SceneManager.LoadScene("End_Scene", LoadSceneMode.Single);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void TogglePaused()
    {
        Pause(!paused);
    }
}
